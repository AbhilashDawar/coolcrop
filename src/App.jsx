import React, { Component } from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter, HashRouter, Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import themeDefault from './theme-default.jsx';
import './App.scss';
import logo from './logo.svg';
import config from './config';
import Home from './containers/home';
import AboutUs from './containers/aboutUs';
import Products from './containers/products';
import Projects from './containers/projects';
import FAQs from './containers/faqs';
import ContactUs from './containers/contactUs';

class App extends Component {

  render() {
    return (
      <div>
        <HashRouter>
          <Switch>
            {/* <Redirect exact from="/" to="/Home" /> */}
            <Route exact path="/" component={Home} />
            <Route exact path="/AboutUs" component={AboutUs} />
            <Route exact path="/Products" component={Products} />
            <Route exact path="/Projects" component={Projects} />
            <Route exact path="/FAQs" component={FAQs} />
            <Route exact path="/contactUs" component={ContactUs} />
          </Switch>
        </HashRouter>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // activeUser: state.activeUser
  };
};

export default connect(
  mapStateToProps
)(App);
