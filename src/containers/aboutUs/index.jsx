import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import './aboutUs.scss';
import Header from '../header';
import Footer from '../footer';
import config from '../../config';

class AboutUs extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Header />
                <div className="aboutUsMainDiv">
                    <Scrollbars>
                        <div className="row aboutUsSection1">
                            <div className="col-md-6">
                                <div className="row aboutUsSectionHeader">
                                    <span>About Us</span>
                                </div>
                                <div className="row sectionParagraph">
                                    <p>CoolCrop creates decentralized, on-farm cold storage solutions for farmers, farmer co-operatives, entrepreneurs and aggregators. This infrastructure is backed by a predictive market analytics and crop management platform to help use the systems more effectively and help the users in daily operations, storage and marketing of their produce.</p>
                                    <p>CoolCrop works with a variety of partners like local organizations, self-help groups, existing farmer co-operatives to engage with the farmers, realize their needs and design appropriate solutions.</p>
                                    <p>CoolCrop is also working with various government agencies and financial institutions to develop sustainable operational and financial models for small and marginal farmers to be able to own, operate and create value from these systems.</p>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <Paper className="sectionImage">
                                    <img src={require('../../assets/Dandeli.jpg')} alt="" height='100%' width='100%' />
                                </Paper>
                            </div>
                        </div>
                        <div className="row aboutUsSection2">
                            {/* <div className="col-md-6">
                                <Paper className="sectionImage">
                                    <img src={require('../../assets/firstSite15.jpg')} alt="" height='100%' width='100%' />
                                </Paper>
                            </div> */}
                            <div className="col-md-6">
                                <div className="row aboutUsSectionHeader">
                                    <span>Mission</span>
                                </div>
                                <div className="row sectionParagraph">
                                    <p>To create sustainable and affordable decentralized cooling systems and smart market analytics tool and ensure that it reaches every small and marginal farmer.</p>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row aboutUsSectionHeader">
                                    <span>Vision</span>
                                </div>
                                <div className="row sectionParagraph">
                                    <p>To enable small and marginal farmers create more value from agriculture by providing them access to the right Technology.</p>
                                </div>
                            </div>
                        </div>
                        <div className="row aboutUsSection3 d-flex justify-content-center">
                            <div className="col-12">
                                {/* <div className="text-center"> */}
                                <span className="aboutUsSectionHeader">Team</span>
                                {/* </div> */}
                            </div>
                            <div className="mx-5">
                                <div className="displayPictures">
                                    <img src={require('../../assets/Niraj Marathe.jpg')} alt="" />
                                </div>
                                <div className="text-center displayNames">
                                    <span className="titles">Niraj Marathe</span>
                                    <br />
                                    <span>Co-Founder</span>
                                </div>
                            </div>
                            <div className="mx-5">
                                <div className="displayPictures">
                                    <img src={require('../../assets/Kendall Nowocin.JPG')} alt="" />
                                </div>
                                <div className="text-center displayNames">
                                    <span className="titles">Kendal Nowocin</span>
                                    <br />
                                    <span>Co-Founder</span>
                                </div>
                            </div>
                            <div className="mx-5">
                                <div className="displayPictures">
                                    <img src={require('../../assets/George Chen.jpg')} alt="" />
                                </div>
                                <div className="text-center displayNames">
                                    <span className="titles">George Chen</span>
                                    <br />
                                    <span>Co-Founder</span>
                                </div>
                            </div>
                            {/* <div className="col-md-6">
                                <Paper className="sectionImage">
                                    <img src={require('../../assets/firstSite15.jpg')} alt="" height='100%' width='100%' />
                                </Paper>
                            </div> */}
                        </div>
                        <div className="row homeSectionSmall">
                            <div className="row partnerLogoSection backgroundGreen">
                                <div className="section1Container">
                                    <div className="section1Data">
                                        <span className="partnerLogoTitle">Partners</span>
                                        <br />
                                        <br />
                                    </div>
                                    <div className="partnerLogoContainer">
                                        <img src={require('../../assets/partnerLogos/battiGhar.png')} alt="" height="100" />
                                        <img src={require('../../assets/partnerLogos/Cosmos-Green.png')} alt="" height="100" />
                                        {/* <img src={require('../../assets/partnerLogos/pwc.png')} alt="" height="100" /> */}
                                        <img src={require('../../assets/partnerLogos/CINI.png')} alt="" height="100" />
                                        <img src={require('../../assets/partnerLogos/rajasthanGovernment.png')} alt="" height="100" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Footer />
                    </Scrollbars>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // activeUser: state.activeUser
    };
};

const mapDispatchToProps = dispatch => ({
    // selectBook: (book) => dispatch(selectBook(book))
});

// export default withRouter(connect(
//     mapStateToProps,
//     mapDispatchToProps,
// )(Key));

export default AboutUs;