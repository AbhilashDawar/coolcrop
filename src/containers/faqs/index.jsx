import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import './faqs.scss';
import Header from '../header';
import Footer from '../footer';
import config from '../../config';

class FAQs extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Header />
                <div className="faqsMainDiv">
                    <Scrollbars>
                        <div className="sampleText">
                            Work in progress.
                        </div>
                        <Footer />
                    </Scrollbars>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // activeUser: state.activeUser
    };
};

const mapDispatchToProps = dispatch => ({
    // selectBook: (book) => dispatch(selectBook(book))
});

// export default withRouter(connect(
//     mapStateToProps,
//     mapDispatchToProps,
// )(Key));

export default FAQs;