import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';

import './contactUs.scss';
import Header from '../header';
import Footer from '../footer';
import config from '../../config';

class ContactUs extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Header />
                <div className="contactUsMainDiv">
                    <Scrollbars>
                        <div className="contactUsSection1">

                        </div>
                        <div className="contactUsSection2">

                        </div>
                        <div className="overlappedFormSection">
                            <Paper className="row formSection">
                                <div className="col-md-4 formLeftPart">
                                    <div className="heading">
                                        <span>Reach us at</span>
                                    </div>
                                    <div className="content">
                                        <div className="row">
                                            <div className="whiteDotMarker">
                                                <img src={require('../../assets/Location.svg')} alt="" height="100%" width="100%" />
                                            </div>
                                            <div className="address">
                                                <span>Coolcrop Technologies Pvt. Ltd.</span><br />
                                                <span>Annapurna, Shastri Pol,</span><br />
                                                <span>Raopura, Varodara 390011</span><br />
                                                <span>Gujarat, INDIA</span>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="whiteDotMarker">
                                            <img src={require('../../assets/Phone.svg')} alt="" height="100%" width="100%" />
                                            </div>
                                            <div className="phone">
                                                <span>+91-99727 67937</span><br />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="whiteDotMarker">
                                            <img src={require('../../assets/Mail.svg')} alt="" height="100%" width="100%" />
                                            </div>
                                            <div className="email">
                                                <span>info@coolcrop.in</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-8 formRightPart">
                                    <div className="rightHeading">
                                        <span>Get in touch</span>
                                    </div>
                                    <div className="content">
                                        <div className="row formRow">
                                            <div className="col-6">
                                                <TextField
                                                    label="Your Name"
                                                    fullWidth={true}
                                                    className="formTextField"
                                                />
                                            </div>
                                            <div className="col-6">
                                                <TextField
                                                    label="Email Address"
                                                    fullWidth={true}
                                                    className="formTextField"
                                                />
                                            </div>
                                        </div>
                                        <div className="row formRow">
                                            <div className="col-6">
                                                <TextField
                                                    label="Company"
                                                    fullWidth={true}
                                                    className="formTextField"
                                                />
                                            </div>
                                            <div className="col-6">
                                                <TextField
                                                    label="Phone"
                                                    fullWidth={true}
                                                    className="formTextField"
                                                />
                                            </div>
                                        </div>
                                        <div className="row formRow">
                                            <div className="col-12">
                                                <TextField
                                                    label="Message"
                                                    fullWidth={true}
                                                    className="formTextField"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Paper>
                        </div>
                        <Footer />
                    </Scrollbars>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // activeUser: state.activeUser
    };
};

const mapDispatchToProps = dispatch => ({
    // selectBook: (book) => dispatch(selectBook(book))
});

// export default withRouter(connect(
//     mapStateToProps,
//     mapDispatchToProps,
// )(Key));

export default ContactUs;