import React from 'react';
// import { connect } from 'react-redux';
// import { withRouter } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import './home.scss';
import Header from '../header';
import Footer from '../footer';
// import config from '../../config';

class Home extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Header />
                <div className="homeMainDiv">
                    <Scrollbars>
                        {/* <div className="homeMainDiv" onWheel={this.scrollAction}> */}
                        <div className="row homeSection justify-content-center">
                            <div className="row section1 strictWidth">
                                <div className="contentSection section1Container">
                                    <div className="section1Data">
                                        <span className="homeSectionHeader">Sustainable, Accessible and Affordable</span>
                                        <br />
                                        <br />
                                        <span className="section1Text">CoolCrop develops decentralized, on-farm cold storages which can be owned and operated by the farmers.</span>
                                        <br />
                                        <br />
                                        <br />
                                        <Button
                                            style={{
                                                "width": "150px",
                                                "textAlign": "center",
                                                "borderRadius": "21px",
                                                "height": "32px",
                                                "color": "white",
                                                "backgroundColor": "#61bf6c"
                                            }}
                                            onClick={() => { this.props.history.push(`/AboutUs`) }}
                                        >Learn More</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row homeSection backgroundGreen justify-content-center">
                            <div className="row section1 strictWidth">
                                <div className="contentSection section1Container">
                                    <div className="section1Data">
                                        <span className="homeSectionHeader">About Us</span>
                                        <br />
                                        <br />
                                        <span className="section1Text">CoolCrop works with farmers, farmer groups and ground organizations to create sustainable cold storage and market analytics solutions at the farm-level.</span>
                                        <br />
                                        <br />
                                        <br />
                                        <Button
                                            style={{
                                                "width": "150px",
                                                "textAlign": "center",
                                                "borderRadius": "21px",
                                                "height": "32px",
                                                "color": "white",
                                                "fontSize": "16px",
                                                "backgroundColor": "#61bf6c"
                                            }}
                                            onClick={() => { this.props.history.push(`/AboutUs`) }}
                                        >Learn More</Button>
                                    </div>
                                </div>
                                {/* <div className="row">
                                    <div className="col-md-6">
                                        <div className="row sectionTitle">
                                            <span>About Us</span>
                                        </div>
                                        <div className="row sectionSubTitle">
                                            <span>CoolCrop works with farmers, farmer groups and ground organizations to create sustainable cold storage and market analytics solutions at the farm-level.</span>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <Paper className="section2Image imageOnBack">
                                            <img src={require('../../assets/firstSite1.jpg')} alt="" height="100%" width="100%" />
                                        </Paper>
                                        <Paper className="section2Image imageOnFront">
                                            <img src={require('../../assets/firstSite3.jpg')} alt="" height="100%" width="100%" />
                                        </Paper>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                        <div className="row homeSection justify-content-center">
                            <div className="row section3 strictWidth">
                                <div className="row contentSection">
                                    <div className="col-12">
                                        <div className="row sectionTitle">
                                            <span>Products</span>
                                        </div>
                                        <div className="row">
                                            <div className="col-3 section3Big">
                                                <div className="row sectionSubTitle">
                                                    <span>Cold Storage Solution</span>
                                                </div>
                                            </div>
                                            <div className="col-3 section3Big">
                                                <div className="row sectionSubTitle">
                                                    <span>Predictive Market Analytics</span>
                                                </div>
                                            </div>
                                            <div className="col-3 section3Big">
                                                <div className="row sectionSubTitle">
                                                    <span>Crop Management Platform</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row homeSectionSmall backgroundDarkGreen justify-content-center">
                            <div className="row section4 strictWidth">
                                {/* <div className="row contentSection"> */}
                                <div className="row">
                                    <div className="col-md-6 section4Details">
                                        <div className="offset-md-4 col-md-8">
                                            <span className="detailsHeader">Cities Served</span><br />
                                            <span className="detailsText">11</span>
                                        </div>
                                    </div>
                                    <div className="col-md-6 section4Details">
                                        <div className="col-md-8">
                                            <span className="detailsHeader">Energy Saved (in kW)</span><br />
                                            <span className="detailsText">1236127</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row homeSectionSmall justify-content-center">
                            <div className="row section1 strictWidth">
                                <div className="contentSection contactUsButtonContainer">
                                    <div className="section1Data">
                                        <Button
                                            style={{
                                                "width": "359px",
                                                "textAlign": "center",
                                                "borderRadius": "21px",
                                                "height": "51px",
                                                "color": "white",
                                                "backgroundColor": "#a9d18e"
                                            }}
                                        >Get In Touch</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row homeSectionSmall justify-content-center">
                            <div className="row partnerLogoSection strictWidth backgroundGreen">
                                <div className="contentSection section1Container">
                                    <div className="section1Data">
                                        <span className="partnerLogoTitle">Supported By</span>
                                        <br />
                                        <br />
                                    </div>
                                    <div className="partnerLogoContainer">
                                        <img src={require('../../assets/partnerLogos/Expo2020.png')} alt="" height="100" />
                                        <img src={require('../../assets/partnerLogos/startUpIndia.png')} alt="" height="100" />
                                        {/* <img src={require('../../assets/partnerLogos/Cosmos-Green.png')} alt="" height="100" />
                                        <img src={require('../../assets/partnerLogos/pwc.png')} alt="" height="100" />
                                        <img src={require('../../assets/partnerLogos/CINI.png')} alt="" height="100" />
                                        <img src={require('../../assets/partnerLogos/rajasthanGovernment.png')} alt="" height="100" /> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Footer />
                    </Scrollbars>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // activeUser: state.activeUser
    };
};

const mapDispatchToProps = dispatch => ({
    // selectBook: (book) => dispatch(selectBook(book))
});

// export default withRouter(connect(
//     mapStateToProps,
//     mapDispatchToProps,
// )(Key));

export default Home;