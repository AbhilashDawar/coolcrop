import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CardActionArea from '@material-ui/core/CardActionArea';
import { Card, CardContent, CardActions, CardMedia } from '@material-ui/core';
import Image from 'react-image-resizer';
import { Carousel } from 'react-responsive-carousel';

import './projects.scss';
import Header from '../header';
import Footer from '../footer';
import config from '../../config';
import CoverflowComponent from '../../components/coverflow/coverflow';

class Projects extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            images: [
                'firstSite1',
                'firstSite2',
                'firstSite3',
                'firstSite4',
                'firstSite5',
                'firstSite6',
                'firstSite7',
                'firstSite8',
                'firstSite9',
                'firstSite10',
                'firstSite11',
                'firstSite12',
                'firstSite13',
                'firstSite14',
                'firstSite15',
                'firstSite16',
                'firstSite17',
                'firstSite18',
                'firstSite19',
                'firstSite20',
                'firstSite21',
                'firstSite22'
            ]
        }

    }

    render() {
        return (
            <div>
                <Header />
                <div className="projectsMainDiv">
                    <Scrollbars>
                        <div className="row projects align-content-center">
                            <div className="col-12 align-content-center font-weight-bold">
                                <span>First Site</span>
                            </div>
                            <div className="project">
                                <CoverflowComponent images={this.state.images} />
                                {/* <Carousel
                                    width="100%"
                                    centerMode
                                    centerSlidePercentage={33}
                                    dynamicHeight={true}
                                    swipeable={true}
                                >
                                    {this.state.images.map((a, index) => {
                                        return <div key={index}>
                                            <img
                                                src={require(`../../assets/${a}.jpg`)}
                                            // height="400px"
                                            // width="400px"
                                            />
                                        </div>
                                    })}
                                </Carousel> */}
                            </div>
                        </div>
                        <Footer />
                    </Scrollbars>
                </div>
            </div >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // activeUser: state.activeUser
    };
};

const mapDispatchToProps = dispatch => ({
    // selectBook: (book) => dispatch(selectBook(book))
});

// export default withRouter(connect(
//     mapStateToProps,
//     mapDispatchToProps,
// )(Key));

export default Projects;