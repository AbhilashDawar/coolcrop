import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
// import { connect } from 'react-redux';
// import { withRouter } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
// import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';

import './products.scss';
import Header from '../header';
import Footer from '../footer';
import { Typography, AppBar } from '@material-ui/core';
// import config from '../../config';

function TabContainer({ children, description, images, type }) {
    return (
        <div>
            <div>
                <div id={`carouselExampleControls${type}`} class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        {images.map((image, index) => {
                            if (index === 0) {
                                return (
                                    <div className="carousel-item active w-100" key={`mainImages${index}`}>
                                        <img class="d-block w-50" src={image} alt="" height="500" />
                                    </div>
                                )
                            } else {
                                return (
                                    <div className="carousel-item w-100" key={`mainImages${index}`}>
                                        <img class="d-block w-50" src={image} alt="" height="500" />
                                    </div>
                                )
                            }
                        })}
                    </div>
                    <a class="carousel-control-prev" href={`#carouselExampleControls${type}`} role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href={`#carouselExampleControls${type}`} role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="clearfix">
                    <div id={`thumbcarousel${type}`} class="carousel slide" data-interval="false">
                        <div class="carousel-inner">
                            {images.map((image, index) => {
                                return (
                                    <div
                                        style={{
                                            border: '1px solid black'
                                        }}
                                        className="item"
                                        key={`thumbnails${index}`}
                                        data-target={`#carouselExampleControls${type}`}
                                        data-slide-to={index}
                                        class="thumb"
                                    >
                                        <img src={image} />
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="descriptionBox">
                    <div className="row title">
                        <div className="">
                            <span>Description</span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="productsContent">
                            <p>
                                {description}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
    tabsRoot: {
        borderBottom: '1px solid #000',
    },
    tabsIndicator: {
        backgroundColor: '#1890ff',
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: 72,
        fontSize: 20,
        marginRight: theme.spacing.unit * 4,
        fontFamily: [
            'Roboto',
            'sans-serif',
        ].join(','),
        '&:hover': {
            color: '#40a9ff',
            opacity: 1,
        },
        '&$tabSelected': {
            color: '#1890ff',
            fontWeight: 'bold',
        },
        '&:focus': {
            color: '#40a9ff',
        },
    },
    tabSelected: {},
    typography: {
        padding: theme.spacing.unit * 3,
    },
});

class Products extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: 0
        };
    }

    handleChange = (event, value) => {
        this.setState({ value });
    };

    handleChangeIndex = index => {
        this.setState({ value: index });
    };

    render() {
        const { classes, theme } = this.props;
        const images = {
            coldStorageSolution: [
                require('../../assets/temp1.png'),
                require('../../assets/temp2.jpg'),
                require('../../assets/temp3.jpg')
            ],
            predictiveMarketAnalytics: [
                require('../../assets/temp1.png'),
                require('../../assets/temp2.jpg'),
                require('../../assets/temp3.jpg')
            ],
            cropManagementPlatform: [
                require('../../assets/temp1.png'),
                require('../../assets/temp2.jpg'),
                require('../../assets/temp3.jpg')
            ]
        };
        const description = {
            coldStorageSolution: `CoolCrop’s control module manages the cooling unit components to maintain
            the desired temperature and conditions with optimum energy utilization.
            The control module can easily be programmed and monitored using an Android
            or iOS phone based interface which enables the user to set the temperatures
            and monitor the parameters.
            Results from the initial pilots have shown energy savings of at least 20%
            over conventional cooling modules.`,
            predictiveMarketAnalytics: `CoolCrop Data Mining and Analytics uses historical data for a
            given location and produce.
            The data from analytics is then put through the proprietary
            forecasting code and presented to farmers in form of a mobile
            application which informs them on expected prices for the storage
            duration of the produce.`,
            cropManagementPlatform: `CoolCrop’s tracking module manages the produce coming into and out of the
            system and provides a holistic farm management tool.
            The module provides alerts on the when the quality of the produce is about
            to change, how long it can be stored, cooling energy consumed, and alerts
            the user if the produce goes outside proper conditions.
            The module links to CoolCrop’s market analytics module to give the user real
            time and future price forecasts to sell or buy produce at a better price.
            It can inform the user of the nearest cold storage unit and buyers or
            sellers for the produce.`
        };
        return (
            <div>
                <Header />
                <div className="productsMainDiv">
                    <Scrollbars>
                        <Tabs
                            classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                            value={this.state.value}
                            onChange={this.handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                            fullWidth
                        >
                            <Tab
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                style={{
                                    height: '100px',
                                }}
                                label="Cold Storage Solution"
                            />
                            <Tab
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                style={{
                                    height: '100px'
                                }}
                                label="Predictive Market Analytics"
                            />
                            <Tab
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                style={{
                                    height: '100px'
                                }}
                                label="Crop Management Platform"
                            />
                        </Tabs>
                        <div className="productsContent">
                            <SwipeableViews
                                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                index={this.state.value}
                                onChangeIndex={this.handleChangeIndex}
                            >
                                <TabContainer
                                    type="1"
                                    images={images.coldStorageSolution}
                                    description={description.coldStorageSolution}
                                />
                                <TabContainer
                                    type="2"
                                    images={images.predictiveMarketAnalytics}
                                    description={description.predictiveMarketAnalytics}
                                />
                                <TabContainer
                                    type="3"
                                    images={images.cropManagementPlatform}
                                    description={description.cropManagementPlatform}
                                />
                            </SwipeableViews>
                        </div>
                        {/* <div className="row productsSection1">
                            <div className="col-md-10">
                                <div className="row sectionHeader">
                                    <span>Cold Storage Solution</span>
                                </div>
                                <div className="row sectionParagraph">
                                    <p>
                                        CoolCrop’s control module manages the cooling unit components to maintain
                                        the desired temperature and conditions with optimum energy utilization.
                                        The control module can easily be programmed and monitored using an Android
                                        or iOS phone based interface which enables the user to set the temperatures
                                        and monitor the parameters.
                                        Results from the initial pilots have shown energy savings of at least 20%
                                        over conventional cooling modules.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="row productsSection2">
                            <div className="col-md-10">
                                <div className="row sectionHeader">
                                    <span>Predictive Market Analytics</span>
                                </div>
                                <div className="row sectionParagraph">
                                    <p>
                                        CoolCrop Data Mining and Analytics uses historical data for a
                                        given location and produce.
                                        The data from analytics is then put through the proprietary
                                        forecasting code and presented to farmers in form of a mobile
                                        application which informs them on expected prices for the storage
                                        duration of the produce.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="row productsSection3">
                            <div className="col-md-10">
                                <div className="row sectionHeader">
                                    <span>About UsCrop Management Platform</span>
                                </div>
                                <div className="row sectionParagraph">
                                    <p>
                                        CoolCrop’s tracking module manages the produce coming into and out of the
                                        system and provides a holistic farm management tool.
                                        The module provides alerts on the when the quality of the produce is about
                                        to change, how long it can be stored, cooling energy consumed, and alerts
                                        the user if the produce goes outside proper conditions.
                                        The module links to CoolCrop’s market analytics module to give the user real
                                        time and future price forecasts to sell or buy produce at a better price.
                                        It can inform the user of the nearest cold storage unit and buyers or
                                        sellers for the produce.
                                    </p>
                                </div>
                            </div>
                        </div> */}
                        <Footer />
                    </Scrollbars>
                </div>
            </div>
        )
    }
}

// Products.propTypes = {
//     classes: PropTypes.object.isRequired,
//     theme: PropTypes.object.isRequired,
// };

// const mapStateToProps = (state) => {
//     return {
//         // activeUser: state.activeUser
//     };
// };

// const mapDispatchToProps = dispatch => ({
//     // selectBook: (book) => dispatch(selectBook(book))
// });

// export default withRouter(connect(
//     mapStateToProps,
//     mapDispatchToProps,
// )(Key));

// export default Products;
export default withStyles(styles, { withTheme: true })(Products);;