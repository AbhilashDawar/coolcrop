import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
// import { selectBook } from '../../actions/selectBook.jsx';
import './footer.scss';
import Logo from "../../components/logo";
import icon_facebook from '../../assets/icon_facebook.svg';
import icon_twitter from '../../assets/icon_twitter.svg';
import icon_linkedIn from '../../assets/icon_linkedIn.svg';
import icon_instagram from '../../assets/icon_instagram.svg';
import config from '../../config';

class Footer extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="footer">
                <div className="row footerMain">
                    <div className="col-md-2 footerLogo">
                        <div className="row align-items-center">
                            <Logo /><span className="companyName">Coolcrop</span>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="row">
                            <div className="col-md-6 footerDetails">
                                <span>Coolcrop Technologies Pvt. Ltd.</span><br />
                                <span>Annapurna, Shastri Pol,</span><br />
                                <span>Raopura, Varodara 390011</span><br />
                                <span>Gujarat, INDIA</span>
                            </div>
                            <div className="col-md-6 footerDetails">
                                <span>+91-99727 67937</span><br />
                                <span>info@coolcrop.in</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="row">
                            <div className="col-1">
                                <img src={icon_facebook} alt="" height={"30px"} />
                            </div>
                            <div className="col-1">
                                <img src={icon_linkedIn} alt="" height={"30px"} />
                            </div>
                            <div className="col-1">
                                <img src={icon_instagram} alt="" height={"30px"} />
                            </div>
                            <div className="col-1">
                                <img src={icon_twitter} alt="" height={"30px"} />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row footerReservedText">
                    <span>&copy;2018.All Rights Reserved</span>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // activeUser: state.activeUser
    };
};

const mapDispatchToProps = dispatch => ({
    // selectBook: (book) => dispatch(selectBook(book))
});

// export default withRouter(connect(
//     mapStateToProps,
//     mapDispatchToProps,
// )(Key));

export default Footer;