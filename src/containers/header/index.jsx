import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
// import { selectBook } from '../../actions/selectBook.jsx';
import './header.scss';
import Logo from "../../components/logo";
import config from '../../config';

class Header extends React.Component {

    constructor(props) {
        super(props);
    }

    changeRoute(link) {
        this.props.history.push(`/${link}`);
    }

    render() {
        return (
            <div className="contentSection header strictWidth">
                <div className="offset-sm-1 col-sm-2">
                    <Logo />
                </div>
                <div className="offset-md-2 col-md-7">
                    <div className="row">
                        <div className="navigator" onClick={() => { this.changeRoute("") }}>
                            <span className="navigatorText">
                                HOME
                            </span>
                        </div>
                        <div className="navigator" onClick={() => { this.changeRoute("AboutUs") }}>
                            <span className="navigatorText">
                                ABOUT US
                            </span>
                        </div>
                        <div className="navigator" onClick={() => { this.changeRoute("Products") }}>
                            <span className="navigatorText">
                                PRODUCTS
                            </span>
                        </div>
                        {/* <div className="navigator" onClick={() => { this.changeRoute("Projects") }}>
                            <span className="navigatorText">
                                PROJECTS
                            </span>
                        </div> */}
                        <div className="navigator" onClick={() => { this.changeRoute("ContactUs") }}>
                            <span className="navigatorText">
                                CONTACT US
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // activeUser: state.activeUser
    };
};

const mapDispatchToProps = dispatch => ({
    // selectBook: (book) => dispatch(selectBook(book))
});

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps,
)(Header));