export default {
    appName: 'Calculator',
    keyTypes: {
        number: "NUMBER",
        actions: "ACTION",
        equate: "EQUATE"
    }
}