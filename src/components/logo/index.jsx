import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import logo from '../../assets/logo.svg';
// import { selectBook } from '../../actions/selectBook.jsx';
import './logo.scss';
import config from '../../config';

class Logo extends React.Component {

    constructor(props) {
        super(props);
    }

    changeRoute(link) {
        this.props.history.push(`/${link}`);
    }

    render() {
        return (
            <div className="col-4 brandLogo" onClick={() => { this.changeRoute("") }}>
                <img src={logo} alt="" height="45px" />
                {/* <span className="brandName" onClick={() => { this.changeRoute("Home") }}>CoolCrop</span> */}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // activeUser: state.activeUser
    };
};

const mapDispatchToProps = dispatch => ({
    // selectBook: (book) => dispatch(selectBook(book))
});

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps,
)(Logo));