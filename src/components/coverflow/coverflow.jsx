import React from 'react';
import ReactDOM from 'react-dom';
import Coverflow from 'react-coverflow';

class CoverflowComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            active: 0
        };
    }

    fn() {

    }

    render() {
        return (
            <div>
                <Coverflow
                    width={960}
                    height={480}
                    displayQuantityOfSide={2}
                    navigation={true}
                    enableHeading={false}
                // active={this.state.active}
                >
                    {this.props.images.map((a, index) => {
                        return <div key={index} style={{ backgroundColor: 'black' }}>
                            <img
                                src={require(`../../assets/${a}.jpg`)}
                                height="200px"
                            // width="200px"
                            />
                        </div>
                    })}
                </Coverflow>
            </div>
        );
    }

    _handleClick() {
        var num = Math.floor((Math.random() * 10) + 1);
        this.setState({
            active: num
        });
    }
};

export default CoverflowComponent;