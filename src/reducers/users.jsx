let users = [
    {
        username: "admin",
        password: "admin",
        type: "ADMIN",
        givenName: "Admin",
        familyName: "The Super Boss",
        isGoogleUser: true
    },
    {
        username: "testUser",
        password: "testUser",
        type: "USER",
        givenName: "Test User",
        familyName: "1",
        imageUrl: "https://image.flaticon.com/icons/svg/16/16480.svg",
        issuedBooks: [],
        favoriteGenre: [],
        isGoogleUser: false
    },
    {
        username: "user",
        password: "user",
        type: "USER",
        givenName: "Test",
        familyName: "User 2",
        imageUrl: "https://image.flaticon.com/icons/svg/16/16480.svg",
        issuedBooks: [],
        favoriteGenre: [],
        isGoogleUser: false
    }
];

export default (state = users, action) => {
    return state;
}
