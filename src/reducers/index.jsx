import { combineReducers } from 'redux';
import Users from './users.jsx';

const reducers = combineReducers({
    users: Users
});

export default reducers;
