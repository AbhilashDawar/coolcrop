import { createMuiTheme } from '@material-ui/core/styles';
import { blue600 } from '@material-ui/core/colors';

const themeDefault = createMuiTheme({
    fontFamily: 'Source Sans Pro',
    palette: {
    },
    appBar: {
        height: 57,
        color: blue600
    },
    raisedButton: {
        primaryColor: blue600,
    }
});

export default themeDefault;